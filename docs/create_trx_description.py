from h5py import File, Group, Dataset
import pandas as pd
import numpy as np
from os.path import basename

f = File('trx.mat')

def arborescence(view, prefix, level):
    print(view.name)
    formatting = '\n' + ('' if level==0 else (level-1)*'    ' + '|___')
    if isinstance(view, Group) or isinstance(view, File):
        for key in view.keys():
            if key == '#refs#':
                continue
            else:
                next_view = view[key]
            prefix = prefix + formatting + key + " :: group"
            prefix = arborescence(next_view, prefix, level+1)
    elif isinstance(view, Dataset):
        if view.dtype == '|O':
            prefix = prefix + formatting  + f"{basename(view.name)} :: dataset :: {view.dtype} :: {view.shape}"
            for ref in view[:].flatten()[:10]:
                d = f[ref]
                prefix = prefix + formatting[:-4]+"    |___"  + f"{d.name} :: dataset :: {d.dtype} :: {d.shape}"
                if d.dtype == '|O':
                    for ref in d[:].flatten()[:10]:
                        d = f[ref]
                        prefix = prefix + formatting[:-4]+"        |___"  + f"{d.name} :: dataset :: {d.dtype} :: {d.shape}"
            prefix = prefix + formatting[:-4]+"    |___"  + '...' # f"... {len(view[:].flatten())-10} more"
        else:
            prefix = prefix + formatting  + f"{basename(view.name)} :: dataset :: {view.dtype} :: {view.shape}"
    else:
        raise ValueError()
    return prefix

print(arborescence(f, "", 0))